//
//  Constants.swift
//  pixelCity
//
//  Created by Som Rith Prohos on 11/13/17.
//  Copyright © 2017 Som Rith Prohos. All rights reserved.
//

import Foundation


let API_KEY = "dd814cba25597ce9702e80fc311953aa"
let API_SECRET = "f41739d76940b5ab"



func flickrUrl(withApiKey key : String , withAnnotation annotation : DroppablePin , andNumberOfPhoto number : Int) -> String {
   return "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(key)&lat=\(annotation.coordinate.latitude)&lon=\(annotation.coordinate.longitude)&radius=1&radius_units=mi&per_page=\(number)&format=json&nojsoncallback=1"
}
