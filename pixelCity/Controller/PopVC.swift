//
//  PopVC.swift
//  pixelCity
//
//  Created by Som Rith Prohos on 11/14/17.
//  Copyright © 2017 Som Rith Prohos. All rights reserved.
//

import UIKit

class PopVC: UIViewController , UIGestureRecognizerDelegate{
    
    
    @IBOutlet weak var popImageView: UIImageView!
    
    var passImage : UIImage!
    
    func initData(forImage image : UIImage) {
        self.passImage = image
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        popImageView.image = passImage
        
        doubleTap()
    }
    
    func doubleTap(){
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(screenWasDoubleTap))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.delegate = self
        view.addGestureRecognizer(doubleTap)
    }
    
    @objc func screenWasDoubleTap() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override var previewActionItems: [UIPreviewActionItem] {
        let likeAction = UIPreviewAction(title: "Like", style: .default) { (action, viewController) -> Void in
            print("You liked the photo")
        }
        
        let deleteAction = UIPreviewAction(title: "Delete", style: .destructive) { (action, viewController) -> Void in
            print("You deleted the photo")
        }
        
        return [likeAction, deleteAction]
    }


}
